# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('akkuvs', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='FormerBattery',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, primary_key=True, serialize=False)),
                ('registration_number', models.CharField(max_length=64, unique=True)),
                ('name', models.CharField(max_length=128)),
                ('voltage', models.DecimalField(decimal_places=2, max_digits=7)),
                ('cells', models.IntegerField(default=1)),
                ('capacity', models.DecimalField(decimal_places=2, max_digits=12)),
                ('registration_date', models.DateTimeField(default=datetime.datetime(1970, 1, 1, 0, 0))),
                ('deletion_date', models.DateTimeField(verbose_name=datetime.datetime(1970, 1, 1, 0, 0))),
                ('deletion_reason', models.TextField(max_length=4096)),
                ('group', models.ForeignKey(null=True, related_name='former_batteries', to='akkuvs.Group')),
                ('size', models.ForeignKey(to='akkuvs.Size')),
                ('technology', models.ForeignKey(to='akkuvs.Technology')),
            ],
        ),
    ]
