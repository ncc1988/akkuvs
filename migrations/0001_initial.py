# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Battery',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('registration_number', models.CharField(max_length=64, unique=True)),
                ('name', models.CharField(max_length=128)),
                ('voltage', models.DecimalField(max_digits=7, decimal_places=2)),
                ('cells', models.IntegerField()),
                ('capacity', models.DecimalField(max_digits=12, decimal_places=2)),
                ('registration_date', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Charge',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('date', models.DateTimeField()),
                ('current', models.DecimalField(max_digits=10, decimal_places=2)),
                ('measured_capacity', models.DecimalField(max_digits=10, decimal_places=2)),
                ('recharged', models.BooleanField(default=False)),
                ('battery', models.ForeignKey(related_name='charges', to='akkuvs.Battery')),
            ],
        ),
        migrations.CreateModel(
            name='Discharge',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('date', models.DateTimeField()),
                ('battery', models.ForeignKey(related_name='discharges', to='akkuvs.Battery')),
            ],
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=128)),
            ],
        ),
        migrations.CreateModel(
            name='Size',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=64, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='State',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('start_date', models.DateTimeField()),
                ('battery', models.ForeignKey(related_name='states', to='akkuvs.Battery')),
            ],
        ),
        migrations.CreateModel(
            name='StateType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
                ('state_class', models.IntegerField(default=0, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Technology',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=64, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Usage',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('location', models.CharField(max_length=128)),
                ('start_date', models.DateTimeField()),
                ('battery', models.ForeignKey(related_name='usages', to='akkuvs.Battery')),
            ],
        ),
        migrations.CreateModel(
            name='UsageType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=64)),
                ('usage_class', models.IntegerField(default=0, unique=True)),
            ],
        ),
        migrations.AddField(
            model_name='usage',
            name='usage_type',
            field=models.ForeignKey(to='akkuvs.UsageType'),
        ),
        migrations.AddField(
            model_name='state',
            name='state_type',
            field=models.ForeignKey(to='akkuvs.StateType'),
        ),
        migrations.AddField(
            model_name='battery',
            name='group',
            field=models.ForeignKey(related_name='batteries', to='akkuvs.Group', null=True),
        ),
        migrations.AddField(
            model_name='battery',
            name='size',
            field=models.ForeignKey(to='akkuvs.Size'),
        ),
        migrations.AddField(
            model_name='battery',
            name='technology',
            field=models.ForeignKey(to='akkuvs.Technology'),
        ),
    ]
