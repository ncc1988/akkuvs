# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('akkuvs', '0002_formerbattery'),
    ]

    operations = [
        migrations.AlterField(
            model_name='battery',
            name='group',
            field=models.ForeignKey(null=True, to='akkuvs.Group', blank=True, related_name='batteries'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='battery',
            name='technology',
            field=models.ForeignKey(related_name='batteries', to='akkuvs.Technology'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='charge',
            name='current',
            field=models.DecimalField(default=0, decimal_places=2, max_digits=10, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='charge',
            name='measured_capacity',
            field=models.DecimalField(default=0, decimal_places=2, max_digits=10, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='formerbattery',
            name='group',
            field=models.ForeignKey(null=True, to='akkuvs.Group', blank=True, related_name='former_batteries'),
            preserve_default=True,
        ),
    ]
