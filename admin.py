from akkuvs.models import Group, Battery, Technology, Size, Charge, Discharge, StateType, State, UsageType, Usage, FormerBattery
from django.contrib import admin

admin.site.register(Group)
admin.site.register(Battery)
admin.site.register(Technology)
admin.site.register(Size)
admin.site.register(Charge)
admin.site.register(Discharge)
admin.site.register(StateType)
admin.site.register(State)
admin.site.register(UsageType)
admin.site.register(Usage)

admin.site.register(FormerBattery)