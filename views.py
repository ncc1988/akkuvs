#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#    AkkuVS – A rechargeable battery management system
#    Copyright (C) 2014  Moritz Strohm <ncc1988@posteo.de>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import render_to_response, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from django.db.models import Max

from akkuvs.models import Group, Battery, Technology, Size, Charge, Discharge, StateType, State, UsageType, Usage



def login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            return redirect(Index)
        else:
            return error(request, message="Your user account is disabled!", template='akkuvs/loginform.xhtml')
    else:
        return error(request, message="Invalid login!", template='akkuvs/loginform.xhtml')


def logout(request):
    logout(request)
    return Index(request)


def login_form(request):
    return render_to_response('akkuvs/loginform.xhtml', context_instance=RequestContext(request))


def error(request, message, template):
    return render_to_response(template, context_instance=RequestContext(request), dictionary={'message': message})


def index(request):
    return render_to_response('akkuvs/main.xhtml',
        {'loggedinUser':request.user,
        'authenticated':request.user.is_authenticated(),
        'groups':Group.objects.all(),
        'ungrouped_batteries':Battery.objects.filter(group=None)
        })

@login_required
def show_battery(request, battery_id):
    try:
        battery = Battery.objects.get(pk=battery_id)
            
        return render_to_response('akkuvs/show_battery.xhtml',
            dictionary={
                'battery':battery
                #'BatteryList':"Test"
                }
            )
    except Battery.DoesNotExist:
        raise HttpResponseNotFound("Battery not found!")

@login_required
def search(request):
    return render_to_response('akkuvs/search.xhtml',
        dictionary={
            'BatteryTechnologies':Technology.objects.all(),
            'BatterySizes':Size.objects.all()
            }
        )


@login_required
def show_list(request):
    return render_to_response('akkuvs/batterylist.xhtml',
        dictionary={
            'BatteryList':Battery.objects.all()
            #'BatteryList':"Test"
            }
        )


@login_required
def new_battery(request):
    return HttpResponse("NewBattery: Not yet implemented!")


@login_required
def new_charge(request, batteryID):
    return HttpResponse("NewCharge: Not yet implemented!")

@login_required
def new_discharge(request, batteryID):
    return HttpResponse("NewDischarge: Not yet implemented!")

@login_required
def new_status(request, batteryID):
    return HttpResponse("NewStatus: Not yet implemented!")

@login_required
def new_usage(request, batteryID):
    return HttpResponse("NewUsage: Not yet implemented!")


def battery_actions(request):
    return HttpResponse("NewUsage: Not yet implemented!")


def show_battery_capacity_svg(request, batteryID):
    b = Battery.objects.get(pk=batteryID)
    latest_charge = b.charges.all().order_by('-date')
    cap = 0
    if(latest_charge):
        latest_charge = latest_charge[0]
        print ("latestCharge.cap="+str(latest_charge.measuredCapacity))
        print ("b.capacity="+str(b.capacity))
        cap = 100*(float(latest_charge.measuredCapacity)/float(b.capacity))
        print ("cap="+str(cap))
        return render_to_response('akkuvs/batterycap.svg',
        dictionary={
            'capacity_percent':str(100.0-cap),
            'unknown_capacity':False,
            }
        )
    else:
        return render_to_response('akkuvs/batterycap.svg',
        dictionary={
            'capacity_percent':0.0,
            'unknown_capacity':True,
            }
        )