
from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    url(r'^$', 'akkuvs.views.index', name='akkuvs-index'),
    url(r'^search$', 'akkuvs.views.search', name='akkuvs-search'),
    url(r'^list$', 'akkuvs.views.show_list', name='akkuvs-show_list'),
    url(r'^battery/(\d+)/$', 'akkuvs.views.show_battery', name='akkuvs-show_battery'),
    url(r'^newbattery$', 'akkuvs.views.new_battery', name='akkuvs-new_battery'),
    url(r'^newcharge/(\d+)/$', 'akkuvs.views.new_charge', name='akkuvs-new_charge'),
    url(r'^newdischarge/(\d+)/$', 'akkuvs.views.new_discharge', name='akkuvs-new_discharge'),
    url(r'^newstatus/(\d+)/$', 'akkuvs.views.new_status', name='akkuvs-new_status'),
    url(r'^newusage/(\d+)/$', 'akkuvs.views.new_usage', name='akkuvs-new_usage'),
    url(r'^battery_actions/$', 'akkuvs.views.battery_actions', name='akkuvs-battery_actions'),
    url(r'^generators/cap/(\d+)/$', 'akkuvs.views.show_battery_capacity_svg', name='akkuvs-show_battery_capacity_svg'),
    
    url(r'^login', 'django.contrib.auth.views.login', {'template_name': 'akkuvs/loginform.xhtml'}, name='akkuvs-login'),
    url(r'^logout', 'django.contrib.auth.views.logout', {'template_name': 'akkuvs/main.xhtml'}, name='akkuvs-logout'),

)